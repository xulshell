// developement options
pref("nglayout.debug.disable_xul_cache", true);
pref("nglayout.debug.disable_xul_fastload", true);

pref("app.update.enabled", false);

pref("toolkit.defaultChromeURI", "chrome://xulshell/content/xulshell.xul");

pref("dom.mozPermissionSettings.enabled", true);
pref("dom.allow_XUL_XBL_for_file", true);
pref("dom.disable_beforeunload", false);
