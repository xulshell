Cu.import("chrome://xulshell/content/modules/signals.jsm");


conlog(signalNamePrefix);

function sigproc (signame, data) {
  conlog("MYSIGNAL(", signame, "): ", data);
  for (let [k, v] of Iterator(data)) conlog(" [", k, "]=[", v, "]");
}

addSignalListener("mysignal", sigproc);
emitSignal("mysignal", {any:42, data:[669], here:"wow!"});
removeSignalListener("mysignal", sigproc);
emitSignal("mysignal", {any:42, data:[669], here:"wow!"});
removeSignalListener("mysignal", sigproc);
