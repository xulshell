function showAlert (msg) {
  let listener = {
    // subject: always null.
    // topic: "alertfinished" when the alert disappears, "alertclickcallback" if the user clicks the text or "alertshow" when the alert is shown.
    // data: The value of the cookie parameter passed into the showAlertNotification method.
    observe: function(subject, topic, data) {
      conlog("subject=", subject, "; topic=", topic, "; data=", data);
    },
  };

  let svc = Cc["@mozilla.org/alerts-service;1"].getService(Ci.nsIAlertsService);
  svc.showAlertNotification(
    // image url
    "chrome://lunarirc/skin/images/logo.png",
    // title
    "Alert title",
    // text
    msg||"hey, i'm an alert!",
    // clickable text?
    true,
    // user cookie
    {},
    // listener
    listener);
}


print(typeof(setTimeout));
print(arguments[0]);

conlog("os: ", hostInfo.os);
conlog("toolkit: ", hostInfo.toolkit);

conlog(getFileFor("profile://a/b/").path);
conlog(getFileFor("profile://").path);

xulshell.dontCloseWindow = true;
showAlert("started...");
setTimeout(function () {
  showAlert("first timer");
  oneShotTimer(function () {
    showAlert("second timer");
    conlog("closing...");
    //xpcshellQuit();
    window.close();
  }, 3500);
  conlog("first timer");
}, 3500);
