//Cu.import("chrome://xulshell/content/modules/debuglog.jsm");
//Cu.import("script://modules/socket.jsm");
Cu.import("chrome://xulshell/content/modules/socket.jsm");
xulshell.dontCloseWindow = true;


let sk = new Object();
sk.__proto__ = Socket;


sk.LOG = function (s) { conlog("*LOG: ", s); };
sk.DEBUG = function (s) { conlog("*DBG: ", s); };
sk.debugLog = true;


sk.onConnection = function () {
  conlog("onConnection");
  this.sendString("GET /boo.zoo HTTP/1.0\r\n");
  this.sendString("\r\n");
};


sk.onConnectionHeard = function () {
  conlog("onConnectionHeard");
};


sk.onConnectionTimedOut = function () {
  conlog("onConnectionTimedOut");
  xpcshellQuit(1);
};


sk.onConnectionReset = function () {
  conlog("onConnectionReset");
  xpcshellQuit(1);
};


sk.onBadCertificate = function (/*boolean*/ aIsSslError, /*AString*/ aNSSErrorMessage) {
  conlog("onBadCertificate(", aIsSslError, "): ", aNSSErrorMessage);
  //xpcshellQuit(1);
};


sk.onConnectionClosed = function () {
  conlog("onConnectionClosed");
  xpcshellQuit(0);
};


sk.onDataReceived = function (/*string*/data, delim) {
  //conlog("onDataReceived: ", data.length, "\n  ", data);
  conlog("*recv: [", data, "]", (delim ? "|": ""));
};


sk.onBinaryDataReceived = function (/*ArrayBuffer*/ data) {
  conlog("onBinaryDataReceived: ", data.length);
  return data.length;
};


sk.sendPing = function () {
  conlog("sendPing");
};


//Sock.onTransportStatus = function (/*nsISocketTransport*/ transport, /*nsresult*/ status, /*unsigned long*/ progress, /*unsigned long*/ total) {
//};


sk.delimiter = "\r\n";
sk.connect("ketmar.no-ip.org", 80);
//sk.connect("google.com", 80);
