/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

////////////////////////////////////////////////////////////////////////////////
Cu.import("resource://gre/modules/XPCOMUtils.jsm");
//Cu.import("resource://gre/modules/Services.jsm");
Cu.import("chrome://xulshell/content/modules/debuglog.jsm");
Cu.import("chrome://xulshell/content/modules/fileio.jsm");


////////////////////////////////////////////////////////////////////////////////
let ioSvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
let dirSvc = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties);


////////////////////////////////////////////////////////////////////////////////
function buildFileURI (aSpec) {
  let dir, path;
  if (aSpec.substr(0, 7) === "script:") {
    dir = getScriptDir();
    path = aSpec.substr(7);
  } else if (aSpec.substr(0, 8) === "profile:") {
    dir = dirSvc.get("ProfD", Ci.nsIFile);
    path = aSpec.substr(8);
  } else if (aSpec.substr(0, 9) === "xulshell:") {
    let mt = aSpec.match(/^xulshell:\/+(.*)$/);
    if (!mt) throw new Error("invalid url: '"+aSpec+"'");
    let uri = ioSvc.newURI("chrome://xulshell/content/modules/mod", null, null);
    uri = Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIChromeRegistry).convertChromeURL(uri);
    let spec = uri.spec;
    if (!(/^file:/.test(spec))) throw new Error("invalid url: '"+aSpec+"'");
    dir = ioSvc.getProtocolHandler("file").QueryInterface(Ci.nsIFileProtocolHandler).getFileFromURLSpec(spec);
    if (!dir) throw new Error("invalid url: '"+aSpec+"'");
    dir = dir.parent; // remove dummy file name
    path = mt[1];
  } else {
    throw new Error("invalid url: '"+aSpec+"'");
  }
  let pos = path.indexOf("?");
  if (pos < 0) pos = path.indexOf("#");
  if (pos >= 0) path = path.substr(0, pos);
  let depth = 0;
  for (let part of path.split("/")) {
    if (!part || part === ".") continue;
    if (part === "..") {
      if (depth < 1) throw new Error("invalid script url: '"+aURI.spec+"'");
      --depth;
      dir = dir.parent;
    } else {
      ++depth;
      dir.append(part);
    }
  }
  return ioSvc.newFileURI(dir);
}


////////////////////////////////////////////////////////////////////////////////
function ScriptProtocol () {}
ScriptProtocol.prototype = {
  classDescription: "Script Protocol Handler",
  classID: Components.ID("{7febb391-6846-4d34-804a-ca48c57fbcb1}"),
  contractID: "@mozilla.org/network/protocol;1?name=script",
  protocolFlags: Ci.nsIProtocolHandler.URI_NOAUTH|
                 Ci.nsIProtocolHandler.URI_NORELATIVE|
                 Ci.nsIProtocolHandler.URI_LOADABLE_BY_ANYONE|
                 Ci.nsIProtocolHandler.URI_DOES_NOT_RETURN_DATA|
                 0,
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIProtocolHandler]),

  newURI: function (aSpec, aOriginCharset, aBaseURI) buildFileURI(aSpec),
  newChannel: function (aURI) false, // to stop loading; `null` will segfault!
};


////////////////////////////////////////////////////////////////////////////////
function ProfileProtocol () {}
ProfileProtocol.prototype = {
  classDescription: "Profile Protocol Handler",
  classID: Components.ID("{7febb391-6846-4d34-804a-ca48c57fbcb2}"),
  contractID: "@mozilla.org/network/protocol;1?name=profile",
  protocolFlags: Ci.nsIProtocolHandler.URI_NOAUTH|
                 Ci.nsIProtocolHandler.URI_NORELATIVE|
                 Ci.nsIProtocolHandler.URI_LOADABLE_BY_ANYONE|
                 Ci.nsIProtocolHandler.URI_DOES_NOT_RETURN_DATA|
                 0,
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIProtocolHandler]),

  newURI: function (aSpec, aOriginCharset, aBaseURI) buildFileURI(aSpec),
  newChannel: function (aURI) false, // to stop loading; `null` will segfault!
};


////////////////////////////////////////////////////////////////////////////////
/*
function XulShellProtocol () {}
XulShellProtocol.prototype = {
  classDescription: "Profile Protocol Handler",
  classID: Components.ID("{7febb391-6846-4d34-804a-ca48c57fbcb3}"),
  contractID: "@mozilla.org/network/protocol;1?name=xulshell",
  protocolFlags: Ci.nsIProtocolHandler.URI_NOAUTH|
                 Ci.nsIProtocolHandler.URI_NORELATIVE|
                 Ci.nsIProtocolHandler.URI_LOADABLE_BY_ANYONE|
                 Ci.nsIProtocolHandler.URI_DOES_NOT_RETURN_DATA|
                 0,
  QueryInterface: XPCOMUtils.generateQI([Ci.nsIProtocolHandler]),

  newURI: function (aSpec, aOriginCharset, aBaseURI) buildFileURI(aSpec),
  newChannel: function (aURI) false, // to stop loading; `null` will segfault!
};
*/


////////////////////////////////////////////////////////////////////////////////
let NSGetFactory = XPCOMUtils.generateNSGetFactory([
  ScriptProtocol
  ,ProfileProtocol
  //,XulShellProtocol
]);
