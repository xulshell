/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
// refresh on next restart
(function () {
  let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;
  Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULRuntime).invalidateCachesOnRestart();
})();


////////////////////////////////////////////////////////////////////////////////
Components.utils.import("chrome://xulshell/content/conwatch.jsm");
