/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
(function () {
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


////////////////////////////////////////////////////////////////////////////////
Cu.import("resource://gre/modules/Services.jsm");
Cu.import("chrome://xulshell/content/modules/debuglog.jsm");
Cu.import("chrome://xulshell/content/modules/fileio.jsm");
Cu.import("chrome://xulshell/content/modules/timers.jsm");


////////////////////////////////////////////////////////////////////////////////
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Error#Custom_Error_Types
function FatalError (msg) {
  this.name = "FatalError";
  this.message = msg||"FATAL ERROR";
}
FatalError.prototype = new Error();
FatalError.prototype.constructor = FatalError;


function QuitError (msg) {
  this.name = "QuitError";
  this.message = msg||"Quit";
}
QuitError.prototype = new Error();
QuitError.prototype.constructor = QuitError;


////////////////////////////////////////////////////////////////////////////////
let errorMessage = "internal error";

function fatal (msg) {
  errorMessage = msg;
  throw new FatalError(msg);
}


function quitAppWithError (msg) {
  //let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;
  if (msg !== undefined && msg !== false && msg !== null) {
    //Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService).alert(null, "XULShell fatal error", ""+msg);
    conlog("FATAL: "+msg+"\n");
  }
  Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
  throw new QuitError();
}


////////////////////////////////////////////////////////////////////////////////
let runJSFile = null; // nsIFile
let scriptArgs = [];
let xulIgnoreSize = false;


function handleArgs (args) {
  if (args.length === 0) quitAppError("internal error!");
  if (!(args[0] instanceof Ci.nsICommandLine)) quitAppError("internal error!");
  args = args[0];
  let noMore = false;
  for (let idx = 0; idx < args.length; ++idx) {
    let arg = args.getArgument(idx);
    if (!arg) continue;
    if (!noMore) {
      //note: xulrunner strips first "-"
      if (arg === "-") { noMore = true; continue; }
      if (arg === "-ignoresize" || arg === "-ignore-size") { xulIgnoreSize = true; continue; }
      if (arg[0] == "-") {
        fatal("unknown argument: '"+arg+"'");
        continue;
      }
    }
    // filename or another arg
    if (scriptArgs.length === 0) {
      // resolve name
      try {
        let fl = getFileFor(arg);
        scriptArgs.push(fl.path);
        runJSFile = fl;
      } catch (e) {
        fatal("can't resolve file: '"+arg+"'");
      }
    } else {
      // just push it
      scriptArgs.push(arg);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
function getSize (dom) {
  let el = dom;
  while (el) {
    if (el.nodeType == 1) {
      if (el.tagName === "window" || el.tagName === "dialog") {
        let w = parseInt(el.getAttribute("width")||"");
        let h = parseInt(el.getAttribute("height")||"");
        let res = {};
        if (!isNaN(w) && w > 0) res.w = w;
        if (!isNaN(h) && h > 0) res.h = h;
        return res;
      }
    }
    el = el.nextSibling;
  }
  el = dom;
  while (el) {
    if (el.firstChild) {
      let res = getSize(el.firstChild);
      if ("w" in res || "h" in res) return res;
    }
    el = el.nextSibling;
  }
  return {};
}


function getSizeFromXUL (src) {
  if (/^data:application\/vnd\.mozilla\.xul\+xml,/.test(src)) src = decodeURIComponent(src.substr(37));
  try {
    let dom = (new DOMParser()).parseFromString(src, "text/xml");
    return getSize(dom);
  } catch (e) {
    //conlog("parse error: ", src);
  }
  return {};
}


function fixWindowFlags (src, flags) {
  flags = flags||"";
  let sz = getSizeFromXUL(src);
  //conlog("w=", sz.w, "; h=", sz.h);
  if (/\bignoresize\b\s*(?:,|$)/.test(flags)) {
    flags = flags.replace(/\bignoresize\b\s*/, "");
    flags = flags.replace(/\bwidth=\d+\s*/, "");
    flags = flags.replace(/\bheight=\d+\s*/, "");
    //flags = flags.replace(/,\s*,/g, ",");
    sz = {};
  }
  if (!(/\bwidth=/.test(flags))) {
    if (flags) flags += ",";
    flags += "width="+("w" in sz ? sz.w : "2");
  }
  if (!(/\bheight=/.test(flags))) {
    if (flags) flags += ",";
    flags += "height="+("h" in sz ? sz.h : "2");
  }
  //conlog("flags=[", flags, "]");
  return flags;
}


////////////////////////////////////////////////////////////////////////////////
let xulshellObj = {
  dontCloseWindow: false,
  get dontCloseMainWindow () (xulshellObj.dontCloseWindow),
  set dontCloseMainWindow (v) (xulshellObj.dontCloseWindow = !!v),

  loadTextContents: loadTextContents,
  fileWriteString: function (fname, str) {
    if (!(fname instanceof Ci.nsIFile)) fname = getFileFor(fname);
    let foStream = Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
    // use 0x02 | 0x10 to open file for appending.
    foStream.init(fname, 0x02|0x08|0x20, 0666, 0);
    // write, create, truncate
    // In a c file operation, we have no need to set file mode with or operation,
    // directly using "r" or "w" usually.
    try {
      // if you are sure there will never ever be any non-ascii text in data you can
      // also call foStream.write(data, data.length) directly
      let converter = Cc["@mozilla.org/intl/converter-output-stream;1"].createInstance(Ci.nsIConverterOutputStream);
      converter.init(foStream, "UTF-8", 0, 0);
      try {
        converter.writeString(str);
      } finally {
        converter.close(); // this closes foStream
      }
    } catch (e) {
      foStream.close();
      throw e;
    }
  },
};


let consoleObj = {
  log: function (msg) conlog(msg),
  warn: function (msg) conlog("*WARN: ", msg),
  warning: function (msg) conlog("*WARN: ", msg),
  info: function (msg) conlog("*INFO: ", msg),
};

try {
  handleArgs(window.arguments);
  if (!runJSFile) {
    //conlog("FUCK!");
    fatal("no file!");
  }
  if (!runJSFile.exists()) {
    //conlog("no file!");
    fatal("file '"+scriptArgs[0]+"' not found!");
  }
  setScriptDir(runJSFile.parent);
  //conlog("FILE: ", runJSFile.path);
  let src = loadTextContents(runJSFile);
  let isxul = /\.xul$/i.test(runJSFile.leafName);

  // create script sandbox
  //let principal = (typeof(window) !== "undefined" ? window : Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal));
  let sbox = new Cu.Sandbox(Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal), {
    sandboxName: "xulshell script",
    sameZoneAs: window,
    wantComponents: true,
    wantXrays: false,
    wantXHRConstructor: true,
    sandboxPrototype: window,
  });

  sbox.Cu = Cu;
  sbox.Cc = Cc;
  sbox.Ci = Ci;
  sbox.Cr = Cr;

  Cu.import("resource://gre/modules/Services.jsm", sbox);
  Cu.import("chrome://xulshell/content/modules/debuglog.jsm", sbox);
  Cu.import("chrome://xulshell/content/modules/fileio.jsm", sbox);
  Cu.import("chrome://xulshell/content/modules/timers.jsm", sbox);

  sbox.print = conlog;
  sbox.xpcshellQuit = function () { quitAppWithError(); };
  sbox.xpcshellQuitMsg = quitAppWithError;
  sbox.quit = function () { quitAppWithError(); };
  sbox.quitMsg = quitAppWithError;
  sbox.arguments = scriptArgs;
  sbox.window = window;
  sbox.document = window.document;

  /*
  Object.defineProperty(sbox, "dontCloseWindow", {
    get: (function () xulshellObj.dontCloseWindow).bind(this),
    set: (function (v) { xulshellObj.dontCloseWindow = !!v; }).bind(this),
  });
  */
  let win = window;

  Object.defineProperty(xulshellObj, "mainWindow", {
    get: function () win,
  });
  Object.defineProperty(xulshellObj, "mainWin", {
    get: function () win,
  });

  Object.defineProperty(sbox, "xulshell", {
    get: (function () xulshellObj).bind(this),
  });

  Object.defineProperty(sbox, "console", {
    get: (function () consoleObj).bind(this),
  });

  // platform and gui
  (function () {
    let ai = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULRuntime);
    let osname = ai.OS;
    if (osname === "WINNT") osname = "Windows";
    sbox.hostInfo = {
      os: osname,
      toolkit: ai.widgetToolkit,
    };
  })();

  let knownManifestDirs = {};

  let flushChromeCaches = function () {
    // refresh registry
    Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIChromeRegistry).checkForNewChrome();
    // flush crome caches
    let os = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
    os.notifyObservers(null, "chrome-flush-caches", null);
  };
  sbox.flushChromeCaches = flushChromeCaches.bind(this);

  sbox.addChromeManifest = function (dir) {
    if (typeof(dir) === "string") dir = getFileFor(dir);
    if (!(dir instanceof Ci.nsIFile)) throw new Error("addChromeManifest: nsIFile expected");
    let path = dir.path;
    if (/\.(manifest|xpi)$/.test(path)) {
      if (!dir.exists() || !dir.isFile() || !dir.isReadable()) throw new Error("addChromeManifest: invalid file expected");
    } else {
      if (!dir.exists() || !dir.isDirectory()) throw new Error("addChromeManifest: non-existing directory expected");
    }
    if (path in knownManifestDirs) {
      // remove it
      delete knownManifestDirs[path];
      Components.manager.removeBootstrappedManifestLocation(dir);
    }
    Components.manager.addBootstrappedManifestLocation(dir);
    knownManifestDirs[path] = true;
    flushChromeCaches();
  };

  let knownResources = {};

  sbox.addResource = function (name, dir) {
    if (typeof(name) !== "string" || !name) throw new Error("addResource: invalid resource name");
    if (typeof(dir) === "string") dir = getFileFor(dir);
    if (!(dir instanceof Ci.nsIFile)) throw new Error("addResource: nsIFile expected");
    if (!dir.exists() || !dir.isDirectory()) throw new Error("addResource: existing directory/jar expected");
    if (!dir.isDirectory() && !dir.isReadable()) throw new Error("addResource: existing directory/jar expected");
    let rh = Services.io.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
    if (name in knownResources) {
      // remove it
      delete knownResources[name];
      rh.setSubstitution(name, null);
    }
    // add new
    let alias = Services.io.newFileURI(dir);
    if (!dir.isDirectory()) alias = Services.io.newURI("jar:"+alias.spec+"!/", null, null);
    rh.setSubstitution(name, alias);
    knownResources[name] = true;
    flushChromeCaches();
  };

  Object.defineProperty(sbox, "scriptDir", {
    get: (function () runJSFile.parent).bind(this),
  });

  errorMessage = null;
  flushChromeCaches();

  let oldOpen = window.open.bind(window);
  window.open = function (url, id="_blank", flags="") {
    conlog("opening window: ", url);
    flags = flags||"";
    if (/^(?:about):/.test(url)) {
      return oldOpen(url, id, flags);
    } else {
      let src = loadTextContents(url);
      flags = fixWindowFlags(src, flags);
      let win = oldOpen("about:blank", id, flags);
      win.document.location = (src.substr(0, 5) !== "data:" ? "data:application/vnd.mozilla.xul+xml,"+encodeURIComponent(src) : src);
      return win;
    }
  };

  window.addEventListener("load", function (e) {
    if (errorMessage) {
      //Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService).alert(null, "XULShell fatal error", ""+errorMessage);
      logError("FATAL: "+errorMessage);
      Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
    }
    // one-shot
    createTimer(function () {
      try {
        if (isxul) {
          let win = oldOpen("about:blank", "_blank", fixWindowFlags(src, "chrome=yes,centerscreen,resizeable"+(xulIgnoreSize ? ",ignoresize" : "")));
          //let win = oldOpen("about:blank", "_blank", "chrome=yes,centerscreen,resizeable,width=2,height=2");
          win.document.location = "data:application/vnd.mozilla.xul+xml,"+encodeURIComponent(src);
          //win.sizeToContent();
          //win.addEventListener("onload", function () { conlog("!"); sizeToContent(); }, false);
          //win.setTimeout(function () { conlog("!"); win.sizeToContent(); }, 1);
        } else {
          Cu.evalInSandbox(src, sbox, "ECMAv5", scriptArgs[0], 1);
        }
      } catch (e) {
        if (e instanceof FatalError) {
          logError("FATAL: ", e.message);
          Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
        } else if (e instanceof QuitError) {
          window.close();
        } else {
          logException("script error", e);
        }
      }
      if (!xulshellObj.dontCloseWindow) {
        window.close();
        //conlog("main window closed");
        //Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
      }
    }, 1, true);
  }, false);

} catch (e) {
  if (e instanceof FatalError) {
    logError("FATAL: ", e.message);
    Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
  } else if (e instanceof QuitError) {
    Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
  } else {
    logException("startup error", e);
    Cc["@mozilla.org/toolkit/app-startup;1"].getService(Ci.nsIAppStartup).quit(Ci.nsIAppStartup.eForceQuit);
  }
}

})();
