/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [
  "createTimer",
  "oneShotTimer",
  "intervalTimer"
];

////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


////////////////////////////////////////////////////////////////////////////
// timer API
// returns timer object
function createTimer (aCallback, aDelay, oneshot) {
  // create the timer object
  let timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
  // The timer object may be garbage collected before it fires, so we need to
  // keep a reference to it alive (https://bugzil.la/647998).
  // However, simply creating a closure over the timer object without using it
  // may not be enough, as the timer might get optimized out of the closure
  // scope (https://bugzil.la/640629#c9). To work around this, the timer object
  // is explicitly stored as a property of the observer.
  let fired = false;
  var observer = {
    observe: function () {
      fired = true;
      // just-in-case check
      if (observer.timer && oneshot) delete observer.timer;
      aCallback();
    },
    timer: timer,
  };
  timer.init(observer, aDelay, (oneshot ? Ci.nsITimer.TYPE_ONE_SHOT : Ci.nsITimer.TYPE_REPEATING_SLACK));
  return {
    get active () !fired,
    get callback () aCallback,
    get oneShot () !!oneShot,
    get interval () aDelay,
    cancel: function () {
      if (observer.timer) {
        observer.timer.cancel();
        delete observer.timer;
      }
    },
  };
}

oneShotTimer = function (aCallback, aDelay) createTimer(aCallback, aDelay, true);
intervalTimer = function (aCallback, aDelay) createTimer(aCallback, aDelay, false);
