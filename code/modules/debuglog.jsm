/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [
  "conlog",
  "logError",
  "logException",
  "setDebugLog"
];

////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

////////////////////////////////////////////////////////////////////////////////
const consvc = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);


////////////////////////////////////////////////////////////////////////////////
let logEnabled = true;

function setDebugLog (nv) {
  let old = logEnabled;
  if (nv !== undefined) logEnabled = !!nv;
  return old;
}


function conlog () {
  if (logEnabled && arguments.length > 0) {
    let s = "";
    for (let idx = 0; idx < arguments.length; ++idx) s += arguments[idx];
    if (s.length) consvc.logStringMessage(s);
  }
}


function logError () {
  if (arguments.length > 0) {
    let s = "";
    for (let idx = 0; idx < arguments.length; ++idx) s += arguments[idx];
    if (s.length) Cu.reportError(s);
  }
}


function logException (msg, e) {
  if (typeof(e) === "undefined" && typeof(msg) === "object") {
    e = msg;
    msg = "SOME";
  }
  if (typeof(e) !== "undefined" /*&& e instanceof global.Error*/ && e) {
    let ss = ""+msg+" ERROR: "+e.name+": "+e.message+" : "+e.lineNumber;
    Cu.reportError(ss);
    if (e.stack) Cu.reportError(e.stack);
    Cu.reportError(e);
  } else {
    Cu.reportError(""+msg);
    if (typeof(e) !== "undefined" && e) Cu.reportError("e="+e);
  }
}
