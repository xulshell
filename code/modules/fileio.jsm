/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [
  "getFileFor",
  "getFileUriFor",
  "loadTextContents",
  "setScriptDir",
  "getScriptDir",
  "getCurrentDir"
];

////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;
let ioSvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
let stdir = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIDirectoryServiceProvider).getFile("CurWorkD", {});
let scriptDir = stdir;


function getCurrentDir () stdir.clone();


function setScriptDir (dir) {
  if (!dir) return;
  if (dir instanceof Ci.nsIFile) scriptDir = dir;
}
function getScriptDir () scriptDir.clone();


////////////////////////////////////////////////////////////////////////////////
// you may pass nsIFile, nsIURI or string
// returns nsIFile
function getFileFor (file) {
  if (typeof(file) === "undefined") throw new Error("path or file object expected");
  if (file instanceof Ci.nsIFile) return file.clone();
  // is it looks like url?
  if (typeof(file) === "string" && /^[^\/]+:/.test(file)) {
    file = ioSvc.newURI(file, null, null);
    if (file.scheme === "chrome") file = Cc["@mozilla.org/chrome/chrome-registry;1"].getService(Ci.nsIChromeRegistry).convertChromeURL(file);
  }
  // process uri
  if (file instanceof Ci.nsIURI) {
    let spec = file.spec;
    if (!(/^file:/.test(spec))) throw new Error("invalid file url: '"+file.spec+"'");
    let res = ioSvc.getProtocolHandler("file").QueryInterface(Ci.nsIFileProtocolHandler).getFileFromURLSpec(spec);
    if (!res) throw new Error("invalid file path: '"+file+"'");
    return res;
  }
  // local file
  if (typeof(file) !== "string") throw new Error("file path is not string");
  if (file && file[0] === "/") {
    // absolute path
    let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
    fl.initWithPath(file);
    return fl;
  } else {
    // relative path
    let cwd = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIDirectoryServiceProvider).getFile("CurWorkD", {});
    for (let part of file.split("/")) {
      if (!part) continue;
      if (part === ".") continue;
      if (part === "..") {
        cwd = cwd.parent;
        if (!cwd) throw new Error("invalid file path: '"+file+"'");
      } else {
        cwd.append(part);
      }
    }
    return cwd;
  }
}


function getFileUriFor (file) ioSvc.newFileURI(getFileFor(file));


////////////////////////////////////////////////////////////////////////////////
// you may pass nsIFile, nsIURI or string
// string will be tried as URI first
function getFileOrUri (file) {
  if (typeof(file) === "undefined") throw new Error("path or file object expected");
  if (file instanceof Ci.nsIFile) return file;
  // try local file first
  try {
    return getFileFor(file);
  } catch (e) {}
  // not a file, try uri
  if (file instanceof Ci.nsIURI) return file;
  if (typeof(file) !== "string") throw new Error("path or file object expected");
  return ioSvc.newURI(file, null, null);
}


////////////////////////////////////////////////////////////////////////////////
// you may pass nsIFile, nsIURI or string
function loadTextContents (path, encoding) {
  if (typeof(encoding) !== "string") encoding = "UTF-8"; else encoding = encoding||"UTF-8";

  function toUnicode (text) {
    if (typeof(text) === "string") {
      if (text.length == 0) return "";
    } else if (text instanceof ArrayBuffer) {
      if (text.byteLength == 0) return "";
      text = new Uint8Array(text);
      return new TextDecoder(encoding).decode(text);
    } else if ((text instanceof Uint8Array) || (text instanceof Int8Array)) {
      if (text.length == 0) return "";
      return new TextDecoder(encoding).decode(text);
    } else {
      return "";
    }
    let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
    converter.charset = encoding;
    let res = converter.ConvertToUnicode(text);
    if (res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3); // fuck BOM
    return res;
  }

  //dump("0:path=["+path+"]\n");
  let file = getFileOrUri(path);
  //dump("1:path=["+path+"]\n");
  if (file instanceof Ci.nsIFile) {
    //dump("file=["+file.path+"]\n");
    // read file
    let inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
    inputStream.init(file, -1, -1, null);
    let scInputStream = Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream);
    scInputStream.init(inputStream);
    let output = scInputStream.read(-1);
    scInputStream.close();
    inputStream.close();
    return toUnicode(output, encoding);
  } else {
    //dump("uri=["+file.spec+"]\n");
    // download from URL
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
    req.mozBackgroundRequest = true;
    req.open("GET", file.spec, false);
    req.timeout = 30*1000;
    req.setRequestHeader("User-Agent", "Mozilla/5.0 NewsFox/0.666");
    const cirq = Ci.nsIRequest;
    req.channel.loadFlags |= cirq.INHIBIT_CACHING|cirq.INHIBIT_PERSISTENT_CACHING|cirq.LOAD_BYPASS_CACHE|cirq.LOAD_ANONYMOUS;
    req.responseType = "arraybuffer";
    //if (sendCookies) addCookies(req, urlsend);
    let error = false;
    req.onerror = function () { error = true; }; // just in case
    req.ontimeout = function () { error = true; }; // just in case
    req.send(null);
    if (!error) {
      error = (req.status != 0 && Math.floor(req.status/100) != 2);
      //(/^file:/.test(file.spec) ? (req.status != 0) : (Math.floor(req.status/100) != 2));
    }
    if (error) throw new Error("can't load URI contents: status="+req.status+"; error="+error);
    return toUnicode(req.response, encoding);
  }
}
