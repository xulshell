/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let EXPORTED_SYMBOLS = [];

const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;
const dumpStr = dump; // from global scope


let ConsoleErrors = {
  dump: function (pfx, str) {
    if (str === undefined) str = "";
    if (typeof(str) !== "string") str = ""+str;
    let count = 0;
    if (pfx !== null) pfx = "["+pfx+"]: ";
    for (let line of str.split("\n")) {
      // replace pfx with spaces
      if (pfx !== null && count === 1) {
        let p = "";
        for (let idx = pfx.length; idx > 0; --idx) p += " ";
        pfx = p;
      }
      line = line.replace(/\s+$/, "");
      if (pfx !== null) {
        dumpStr(pfx+line+"\n");
      } else {
        dumpStr(line+"\n");
      }
      ++count;
    }
  },

  dumpMsg: function (msg) {
    // long long timeStamp
    // wstring message
    //this.dump("INFO", msg.message.replace(/\s+$/, ""));
    this.dump(null, msg.message.replace(/\s+$/, ""));
  },

  dumpErr: function (msg) {
    if (msg.category === "chrome registration") return;
    let pfx = "ERRR";
    if (msg.flags&Ci.nsIScriptError.exceptionFlag) pfx = "EXCP";
    else if (msg.flags&Ci.nsIScriptError.strictFlag) pfx = "STRI";
    else if (msg.flags&Ci.nsIScriptError.warningFlag) pfx = "WARN";
    let mstr = "";
    if (msg.sourceName === "chrome://xulshell/content/modules/debuglog.jsm") {
    } else {
      mstr += (msg.sourceName||"<somefile>")+" ("+msg.lineNumber+":"+msg.columnNumber+") ["+msg.category+"]\n";
    }
    //mstr += mstr.msg.sourceLine;
    mstr += msg.errorMessage.replace(/\s+$/, "");
    this.dump(pfx, mstr);
    /**
     * Categories I know about -
     * XUL javascript
     * content javascript (both of these from nsDocShell, currently)
     * system javascript (errors in JS components and other system JS)
     */
    //readonly attribute string category;
  },

  observe: function (object) {
    try {
      if (object instanceof Ci.nsIScriptError) {
        //dump("***********\n");
        this.dumpErr(object);
      } else {
        this.dumpMsg(object);
      }
    } catch (e) {}
  },
};

Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService).registerListener(ConsoleErrors);

// remove
//Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService).unregisterListener(this);
